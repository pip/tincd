# Tinc con parche de anonimato!

Cuando un nodo tiene `IndirectData` activado, los nodos con los que se
publica anuncian su IP pública de todas formas.  Con este parche no se
distribuye la IP y los nodos con datos indirectos permanecen anónimos.

