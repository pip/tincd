# Maintainer: Johannes Löthberg <johannes@kyriasis.com>
# Contributor: Timothy Redaelli <timothy.redaelli@gmail.com>
# Contributor: helios
# Contributor: Lothar Gesslein
# Contributor: Dominik George <nik@naturalnet.de>
# Contributor: René Montes <renemontes@partidopirata.com.ar>

pkgname=tinc
pkgver=1.0.36
pkgrel=1

pkgdesc="VPN (Virtual Private Network) daemon"
url="https://www.tinc-vpn.org/"
arch=('x86_64')
license=('GPL')

depends=('lzo' 'openssl' 'zlib')

source=(https://www.tinc-vpn.org/packages/tinc-$pkgver.tar.gz{,.sig}
        anonymous.patch)

validpgpkeys=('D62BDD168EFBE48BC60E8E234A6084B9C0D71F4A')

prepare() {
  cd tinc-$pkgver
  patch -Np 0 -i "$srcdir/anonymous.patch"
}

build() {
  cd tinc-$pkgver

  ./configure \
    --prefix=/usr \
    --sysconfdir=/etc \
    --localstatedir=/var \
    --sbindir=/usr/bin \
    --with-systemd=/usr/lib/systemd/system
  make
}

package() {
  cd tinc-$pkgver

  make DESTDIR="$pkgdir/" install
  install -dm755 "$pkgdir"/usr/share/tinc/examples
  cp -a doc/sample-config/. "$pkgdir"/usr/share/tinc/examples/
  find "$pkgdir"/usr/share/tinc/examples -type f -exec chmod 644 {} +
  find "$pkgdir"/usr/share/tinc/examples -type d -exec chmod 755 {} +
}
sha512sums=('23af9162f7ae700bad01e1f59f23f32d1b183b185ec35f4a69a987c52c53cfebfa9e852203b399f035988078e9131e5d59b018554a52a30044f34df6e64b5289'
            'SKIP'
            '97c65c211839f89bfb6b9214f25f64284622add376a2900fdd9b400deed4cef38bf1ecdab1daada10f3d46f4d11cfc15596033d410f28544f14b4aa0a995861f')
